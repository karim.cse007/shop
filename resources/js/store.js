import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        categories:[],
        logoName:{},
        social:{},
        contact:{},
        products:[],
    },
    getters:{
        categories: state => {
            return state.categories;
        },
        logoName: state => {
            return state.logoName;
        },
        social: state => {
            return state.social;
        },
        contact: state => {
            return state.contact;
        },
        products: state => {
            return state.products;
        },
    },
    mutations: {
        async setCategories (state) {
            await axios.get('/header/all/info').then((response)=>{
                let data = response.data;
                state.categories=data.categories;
                state.logoName=data.logoName;
                state.social=data.social;
                state.contact=data.contact;
            }).catch((error)=>{
                this.categories= [];
                this.logoName={};
                state.social={};
                state.contact={};
            });
        },
        async setAllProducts (state) {
            await axios.get('/products/all/info').then((response)=>{
                state.products=response.data;
            }).catch((error)=>{
                state.products=[];
            });
        },
    },
    actions: {
        getCategories (context) {
            context.commit('setCategories');
        },
        getProducts(context){
            context.commit('setAllProducts');
        }
    },
    modules: {

    }
})
export default store;
