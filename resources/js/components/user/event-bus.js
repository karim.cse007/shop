import Vue from 'vue';
export const EventBus = new Vue();

/***
 // leftbar.vue
 import { EventBus } from './event-bus.js';

 export default {
  methods: {
    onClick(value) {
     EventBus.$emit('click::event', value);
   }
 }
}

 // applicants.vue
 import { EventBus } from './event-bus.js';

 EventBus.$on('click::event', payload => {
   console.log(payload)
});

 ****/
