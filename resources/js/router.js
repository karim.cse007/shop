import Vue from 'vue';
import VueRouter from "vue-router";
import HomeComponent from "./components/user/HomeComponent";
import SingleProduct from "./components/user/SingleProduct";
Vue.use(VueRouter);

export default new VueRouter({
    routes:[
        /*Dashboard related router*/
        { path: '/', component: HomeComponent, name:'home'},
        { path: '/single/product', component: SingleProduct, name:'single.product',props:true},

    ],
    mode:'history',
});
