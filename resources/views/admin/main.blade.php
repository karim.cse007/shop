@extends('layouts.backend.app')

@section('title','Dashboard')

@push('css')

@endpush
@section('content')
    <admin-main></admin-main>
@endsection

@push('js')
    <script src="{{asset('assets/backend/js/sweetalert.js')}}"></script>
@endpush
