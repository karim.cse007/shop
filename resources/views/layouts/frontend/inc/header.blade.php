<header>
    <div class="mobile-fix-option"></div>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact">
                        <ul>
                            <li>Welcome to Our store Multikart</li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i>Call Us: 123 - 456 - 7890</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <ul class="header-dropdown">
                        <li class="mobile-wishlist"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        </li>
                        <li class="onhover-dropdown mobile-account"><i class="fa fa-user" aria-hidden="true"></i>
                            My Account
                            <ul class="onhover-show-div">
                                <li><a href="#" data-lng="en">Login</a></li>
                                <li><a href="#" data-lng="es">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-menu">
                    <div class="menu-left">
                        <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                        <div class="brand-logo">
                            <a href="#">
                                <img src="{{asset('assets/frontend/images/icon/logo.png')}}"
                                     class="img-fluid blur-up lazyload" alt=""></a>
                        </div>
                    </div>
                    <div class="menu-right pull-right">
                        <div>
                            <nav id="main-nav">
                                <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                                    <li>
                                        <div class="mobile-back text-right">Back<i class="fa fa-angle-right pl-2"
                                                                                   aria-hidden="true"></i></div>
                                    </li>
                                    <li class="mega" id="hover-cls">
                                        <a href="javascript:void(0)">All Categories</a>
                                        <ul class="mega-menu full-mega-menu">
                                            <li>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>portfolio</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="grid-2-col.html">portfolio grid
                                                                                2</a></li>
                                                                        <li><a href="grid-3-col.html">portfolio grid
                                                                                3</a></li>
                                                                        <li><a href="grid-4-col.html">portfolio grid
                                                                                4</a></li>
                                                                        <li><a href="masonary-2-grid.html">mesonary
                                                                                grid 2</a></li>
                                                                        <li><a href="masonary-3-grid.html">mesonary
                                                                                grid 3</a></li>
                                                                        <li><a href="masonary-4-grid.html">mesonary
                                                                                grid 4</a></li>
                                                                        <li><a href="masonary-fullwidth.html">mesonary
                                                                                full width</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>add to cart</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="nursery.html">cart modal
                                                                                popup</a></li>
                                                                        <li><a href="vegetables.html">qty. counter
                                                                                <i class="fa fa-bolt icon-trend"
                                                                                   aria-hidden="true"></i></a></li>
                                                                        <li><a href="bags.html">cart top</a></li>
                                                                        <li><a href="shoes.html">cart bottom</a>
                                                                        </li>
                                                                        <li><a href="watch.html">cart left</a></li>
                                                                        <li><a href="tools.html">cart right</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>theme elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="element-title.html">title</a>
                                                                        </li>
                                                                        <li><a href="element-banner.html">collection
                                                                                banner</a></li>
                                                                        <li><a href="element-slider.html">home
                                                                                slider</a></li>
                                                                        <li><a
                                                                                href="element-category.html">category</a>
                                                                        </li>
                                                                        <li><a
                                                                                href="element-service.html">service</a>
                                                                        </li>
                                                                        <li><a href="element-image-ratio.html">image
                                                                                size ratio <i
                                                                                    class="fa fa-bolt icon-trend"
                                                                                    aria-hidden="true"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>product elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li class="up-text"><a
                                                                                href="element-productbox.html">product
                                                                                box<span>10+</span></a></li>
                                                                        <li><a href="element-product-slider.html">product
                                                                                slider</a></li>
                                                                        <li><a href="element-no_slider.html">no
                                                                                slider</a></li>
                                                                        <li><a href="element-mulitiple_slider.html">multi
                                                                                slider</a></li>
                                                                        <li><a href="element-tab.html">tab</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>email template </h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="email-order-success.html">order
                                                                                success</a></li>
                                                                        <li><a href="email-order-success-two.html">order
                                                                                success 2</a></li>
                                                                        <li><a href="email-template.html">email
                                                                                template</a></li>
                                                                        <li><a href="email-template-two.html">email
                                                                                template 2</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="mega" id="hover-cls">
                                        <a href="javascript:void(0)">Brands</a>
                                        <ul class="mega-menu full-mega-menu">
                                            <li>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>portfolio</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="grid-2-col.html">portfolio grid
                                                                                2</a></li>
                                                                        <li><a href="grid-3-col.html">portfolio grid
                                                                                3</a></li>
                                                                        <li><a href="grid-4-col.html">portfolio grid
                                                                                4</a></li>
                                                                        <li><a href="masonary-2-grid.html">mesonary
                                                                                grid 2</a></li>
                                                                        <li><a href="masonary-3-grid.html">mesonary
                                                                                grid 3</a></li>
                                                                        <li><a href="masonary-4-grid.html">mesonary
                                                                                grid 4</a></li>
                                                                        <li><a href="masonary-fullwidth.html">mesonary
                                                                                full width</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>add to cart</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="nursery.html">cart modal
                                                                                popup</a></li>
                                                                        <li><a href="vegetables.html">qty. counter
                                                                                <i class="fa fa-bolt icon-trend"
                                                                                   aria-hidden="true"></i></a></li>
                                                                        <li><a href="bags.html">cart top</a></li>
                                                                        <li><a href="shoes.html">cart bottom</a>
                                                                        </li>
                                                                        <li><a href="watch.html">cart left</a></li>
                                                                        <li><a href="tools.html">cart right</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>theme elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="element-title.html">title</a>
                                                                        </li>
                                                                        <li><a href="element-banner.html">collection
                                                                                banner</a></li>
                                                                        <li><a href="element-slider.html">home
                                                                                slider</a></li>
                                                                        <li><a
                                                                                href="element-category.html">category</a>
                                                                        </li>
                                                                        <li><a
                                                                                href="element-service.html">service</a>
                                                                        </li>
                                                                        <li><a href="element-image-ratio.html">image
                                                                                size ratio <i
                                                                                    class="fa fa-bolt icon-trend"
                                                                                    aria-hidden="true"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>product elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li class="up-text"><a
                                                                                href="element-productbox.html">product
                                                                                box<span>10+</span></a></li>
                                                                        <li><a href="element-product-slider.html">product
                                                                                slider</a></li>
                                                                        <li><a href="element-no_slider.html">no
                                                                                slider</a></li>
                                                                        <li><a href="element-mulitiple_slider.html">multi
                                                                                slider</a></li>
                                                                        <li><a href="element-tab.html">tab</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>email template </h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="email-order-success.html">order
                                                                                success</a></li>
                                                                        <li><a href="email-order-success-two.html">order
                                                                                success 2</a></li>
                                                                        <li><a href="email-template.html">email
                                                                                template</a></li>
                                                                        <li><a href="email-template-two.html">email
                                                                                template 2</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="mega" id="hover-cls"><a href="#">features
                                            <div class="lable-nav">new</div>
                                        </a>
                                        <ul class="mega-menu full-mega-menu">
                                            <li>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>portfolio</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="grid-2-col.html">portfolio grid
                                                                                2</a></li>
                                                                        <li><a href="grid-3-col.html">portfolio grid
                                                                                3</a></li>
                                                                        <li><a href="grid-4-col.html">portfolio grid
                                                                                4</a></li>
                                                                        <li><a href="masonary-2-grid.html">mesonary
                                                                                grid 2</a></li>
                                                                        <li><a href="masonary-3-grid.html">mesonary
                                                                                grid 3</a></li>
                                                                        <li><a href="masonary-4-grid.html">mesonary
                                                                                grid 4</a></li>
                                                                        <li><a href="masonary-fullwidth.html">mesonary
                                                                                full width</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>add to cart</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="nursery.html">cart modal
                                                                                popup</a></li>
                                                                        <li><a href="vegetables.html">qty. counter
                                                                                <i class="fa fa-bolt icon-trend"
                                                                                   aria-hidden="true"></i></a></li>
                                                                        <li><a href="bags.html">cart top</a></li>
                                                                        <li><a href="shoes.html">cart bottom</a>
                                                                        </li>
                                                                        <li><a href="watch.html">cart left</a></li>
                                                                        <li><a href="tools.html">cart right</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>theme elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="element-title.html">title</a>
                                                                        </li>
                                                                        <li><a href="element-banner.html">collection
                                                                                banner</a></li>
                                                                        <li><a href="element-slider.html">home
                                                                                slider</a></li>
                                                                        <li><a
                                                                                href="element-category.html">category</a>
                                                                        </li>
                                                                        <li><a
                                                                                href="element-service.html">service</a>
                                                                        </li>
                                                                        <li><a href="element-image-ratio.html">image
                                                                                size ratio <i
                                                                                    class="fa fa-bolt icon-trend"
                                                                                    aria-hidden="true"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>product elements</h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li class="up-text"><a
                                                                                href="element-productbox.html">product
                                                                                box<span>10+</span></a></li>
                                                                        <li><a href="element-product-slider.html">product
                                                                                slider</a></li>
                                                                        <li><a href="element-no_slider.html">no
                                                                                slider</a></li>
                                                                        <li><a href="element-mulitiple_slider.html">multi
                                                                                slider</a></li>
                                                                        <li><a href="element-tab.html">tab</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col mega-box">
                                                            <div class="link-section">
                                                                <div class="menu-title">
                                                                    <h5>email template </h5>
                                                                </div>
                                                                <div class="menu-content">
                                                                    <ul>
                                                                        <li><a href="email-order-success.html">order
                                                                                success</a></li>
                                                                        <li><a href="email-order-success-two.html">order
                                                                                success 2</a></li>
                                                                        <li><a href="email-template.html">email
                                                                                template</a></li>
                                                                        <li><a href="email-template-two.html">email
                                                                                template 2</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">blog</a>
                                        <ul>
                                            <li><a href="blog-page.html">left sidebar</a></li>
                                            <li><a href="blog(right-sidebar).html">right sidebar</a></li>
                                            <li><a href="blog(no-sidebar).html">no sidebar</a></li>
                                            <li><a href="blog-details.html">blog details</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div>
                            <div class="icon-nav">
                                <ul>
                                    <li class="onhover-div mobile-search">
                                        <div><img src="{{asset('assets/frontend/images/icon/search.png')}}"
                                                  onclick="openSearch()"
                                                  class="img-fluid blur-up lazyload" alt=""> <i class="ti-search"
                                                                                                onclick="openSearch()"></i>
                                        </div>
                                        <div id="search-overlay" class="search-overlay">
                                            <div> <span class="closebtn" onclick="closeSearch()"
                                                        title="Close Overlay">×</span>
                                                <div class="overlay-content">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <form>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control"
                                                                               id="exampleInputPassword1"
                                                                               placeholder="Search a Product">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-primary"><i
                                                                            class="fa fa-search"></i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="onhover-div mobile-setting">
                                        <div><img src="{{asset('assets/frontend/images/icon/setting.png')}}"
                                                  class="img-fluid blur-up lazyload" alt=""> <i
                                                class="ti-settings"></i></div>
                                        <div class="show-div setting">
                                            <h6>language</h6>
                                            <ul>
                                                <li><a href="#">english</a></li>
                                                <li><a href="#">french</a></li>
                                            </ul>
                                            <h6>currency</h6>
                                            <ul class="list-inline">
                                                <li><a href="#">euro</a></li>
                                                <li><a href="#">rupees</a></li>
                                                <li><a href="#">pound</a></li>
                                                <li><a href="#">doller</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="onhover-div mobile-cart">
                                        <div onclick="openCart()"><img src="{{asset('assets/frontend/images/icon/cart.png')}}"
                                                  class="img-fluid blur-up lazyload" alt=""> <i
                                                class="ti-shopping-cart"></i></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header end -->
