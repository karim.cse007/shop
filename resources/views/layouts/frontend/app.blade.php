<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') -{{ config('', 'Online shop') }}</title>
    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{asset('assets/favico.png')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/favico.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/favico.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/favico.png')}}">

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/fontawesome.css')}}">

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/slick-theme.css')}}">

    <!-- Animate icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/animate.css')}}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/themify-icons.css')}}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Theme css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/color1.css')}}" media="screen" id="color">
    <!-- Scripts -->

    @stack('css')
</head>
<body>
<div id="app">
    <header-frontend :current-user="{{ json_encode(['isLoggedIn'=>auth()->check()?true:false, 'user'=>auth()->check() ? auth()->user() : null]) }}"></header-frontend>
    <section class="content">
        @yield('content')
    </section>
    <footer-frontend></footer-frontend>
</div>

<!-- tap to top -->
<div class="tap-top top-cls">
    <div>
        <i class="fa fa-angle-double-up"></i>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('assets/frontend/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.exitintent.js')}}"></script>
<script src="{{asset('assets/frontend/js/exit.js')}}"></script>
<!-- popper js-->
<script src="{{asset('assets/frontend/js/popper.min.js')}}"></script>

<!-- slick js-->
<script src="{{asset('assets/frontend/js/slick.js')}}" ></script>
    <!-- menu js-->
<script src="{{asset('assets/frontend/js/menu.js')}}" ></script>

<!-- lazyload js-->
<script src="{{asset('assets/frontend/js/lazysizes.min.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{asset('assets/frontend/js/bootstrap.js')}}"></script>

<!-- Bootstrap Notification js-->
<script src="{{asset('assets/frontend/js/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/fly-cart.js')}}"></script>
<!-- Theme js-->
<!-- <script src="{{asset('assets/frontend/js/jquery.elevatezoom.js')}}"></script> -->
@stack('js')
<script src="{{asset('assets/frontend/js/script.js')}}"></script>
</body>
</html>
