@extends('layouts.frontend.app')

@section('title','Muskans 100 gifts')

@push('css')

@endpush
@section('content')
    <main-component :current-user="{{ json_encode(['isLoggedIn'=>auth()->check() ? true : false, 'user'=>auth()->check() ? auth()->user() : null]) }}"></main-component>
@endsection

@push('js')

@endpush
