<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','discount','expire',
    ];
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
