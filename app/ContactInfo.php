<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    protected $fillable=['email','phone1','phone2','address','map_location'];
    protected $hidden=['created_at','updated_at'];
}
