<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','code','category_id','sub_category_id','sub_sub_category_id',
        'brand_id','description','condition','image','price','is_available','is_disable',
    ];
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }
    public function subSubCategory(){
        return $this->belongsTo(SubSubCategory::class);
    }
    public function sizes(){
        return $this->belongsToMany(Size::class)->withTimestamps();
    }
    public function colors(){
        return $this->belongsToMany(Color::class)->withTimestamps();
    }
    public function discount(){
        return $this->hasOne(Discount::class);
    }
    public  function reviews(){
        return $this->hasMany(Review::class);
    }
}
