<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','is_disable'
    ];
    public function products(){
        return $this->hasMany(Product::class);
    }
    public function sub_categories(){
        return $this->belongsToMany(SubCategory::class)->withTimestamps();
    }
    public function sub_sub_categories(){
        return $this->hasMany(SubSubCategory::class);
    }
}
