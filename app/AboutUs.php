<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $fillable=['title','image','description'];
    protected $hidden=['created_at','updated_at'];

}
