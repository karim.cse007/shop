<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PasswordChangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /*
     * methods for forget password
     * */
    public function changePassword(Request $request){
        Validator::make($request->all(),[
            'old_password'=>'required',
            'password'=>'required|confirmed|min:6'
        ])->validate();
        $hashpassword = auth()->user()->password;

        if(Hash::check($request->old_password, $hashpassword)){
            if (!Hash::check($request->password,$hashpassword)){
                auth()->user()->update(['password'=>Hash::make($request->password)]);
                $msg = "<span style='color:green;'>Success !<strong> Successfully change the password</strong></span>";
                return response()->json($msg,200);
            }else{
                $msg ="<span style='color:red;'>Error! <strong>New password cannot be the same as old password</strong></span>";
                return response()->json($msg,200);
            }
        }else{
            $msg ="<span style='color:red;'>Error! <strong>old password not match!!</strong></span>";
            return response()->json($msg,200);
        }
    }
}
