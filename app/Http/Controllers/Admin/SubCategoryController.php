<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubCategoryController extends Controller
{
    public function index(){
        $sub_categories = SubCategory::all();
        $cats =[];
        foreach ($sub_categories as $cat){
            $cat->products->count();
            $cat->categories;
            $cats[]=$cat;
        }
        return response()->json($cats,200);
    }
    public function add(Request $request){
        Validator::make($request->all(),[
            'name'=>'required|string|unique:sub_categories,name|max:191',
            'categories'=>'required|not_in:0|exists:categories,id',
        ])->validate();
        $sub_category = new SubCategory();
        $sub_category->name=$request->name;
        $ss = $sub_category->save();
        $sub_category->categories()->attach($request->categories);
        if (isset($ss)) return response()->json('Successfully Added',201);
        return response()->json('invalid request',404);
    }
    public function update(Request $request,SubCategory $subCategory){
        Validator::make($request->all(),[
            'name'=>'required|string|max:191',
            'categories'=>'required|not_in:0|exists:categories,id',
        ])->validate();
        $subCategory->name=$request->name;
        $ss = $subCategory->save();
        $subCategory->categories()->sync($request->categories);
        if (isset($ss)) return response()->json('Successfully update',201);
        return response()->json('invalid request',404);
    }
    public function remove(SubCategory $sub_category){
        //return response()->json([1,$sub_category],200);
        if ($sub_category->products->count()>0)
            return response()->json([0,"This Sub-Category has already product you can't remove this."]);
        else if ($sub_category->sub_sub_categories->count()>0)
            return response()->json([0,"This Sub-Category already has sub sub category you can't remove this."]);
        $sub_category->categories()->detach();
        $sub_category->delete();
        return response()->json([1,"Successfully Delete"],200);
    }
}
