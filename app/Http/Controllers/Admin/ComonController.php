<?php

namespace App\Http\Controllers\Admin;

use App\AboutUs;
use App\ContactInfo;
use App\Http\Controllers\Controller;
use App\LogoName;
use App\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ComonController extends Controller
{
    public function index(){
        return view('admin.main');
    }
    public function logoName(){
        $logoName=LogoName::first();
        return response()->json($logoName,200);
    }
    public function logoNameUpdate(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ]);
        if ($val->fails()){
            return 'fail';
        }
        $image = $request->file('image');
        $logoName = LogoName::first();
        if (!isset($logoName->id)){
            $logoName=new LogoName();
        }
        if (isset($image)){
            $imgName= 'logo'.'.'.$image->getClientOriginalExtension();
            $img = Image::make($image)->resize(200,100)->save();
            Storage::disk('public')->put($imgName,$img);
        }
        $logoName->name=$request->name;
        if(isset($imgName)) $logoName->logo='/storage/'.$imgName;
        $logoName->save();
        return response()->json('Successfully added',200);
    }
    public function social(){
        $social = Social::first();
        return response()->json($social,200);
    }

    /*
     * method for social info update
     * */
    public function socialUpdate(Request $request){
        Validator::make($request->all(),[
           'facebook'=>'required|string|max:191',
           'tweeter'=>'required|string|max:191',
           'linkedin'=>'required|string|max:191',
           'instagram'=>'required|string|max:191',
           'youtube'=>'required|string|max:191',
           'skype'=>'required|string|max:191',
        ])->validate();
        $social = Social::first();
        if (!isset($social->id)){
            $social = new Social();
        }
        $social->facebook=$request->facebook;
        $social->tweeter=$request->tweeter;
        $social->linkedin=$request->linkedin;
        $social->instagram=$request->instagram;
        $social->youtube =$request->youtube;
        $social->skype=$request->skype;
        $social->save();
        return response()->json('Successfully updated',200);
    }

    /*
     * method for about us information get
     * */
    public function aboutUs(){
        $about = AboutUs::first();
        return response()->json($about,200);
    }

    /*
     * method for about us information update
     * */
    public function aboutUsUpdate(Request $request){
        Validator::make($request->all(),[
           'title'=>'required|string|max:191',
           'description'=>'required|string',
        ])->validate();

        $about = AboutUs::first();
        $image = $request->file('image');
        if (isset($image)){
            $imgName='aboutUs'.'.'.$image->getClientOriginalExtension();
            $img = Image::make($image)->resize(400,600)->save();
            Storage::disk('public')->put($imgName,$img);
        }
        $info=['title'=>$request->title,'description'=>$request->description];
        if (isset($imgName)){
            $info +=['image'=>'/storage/'.$imgName];
        }
        if (isset($about->id)) $about->update($info);
        else AboutUs::create($info);
        return response()->json('Successfully Updated',200);
    }
    /*
     * method for contact information get
     * */
    public function contactInfo(){
        $contact=ContactInfo::first();
        return response()->json($contact,200);
    }
    /*
     * method for contact information update
     * */
    public function contactUpdate(Request $request){
        Validator::make($request->all(),[
            'phone1'=>'required|string|max:191',
            'phone2'=>'required|string|max:191',
            'email'=>'required|string|max:191|email',
            'address'=>'required|string',
            'map_location'=>'required|string',
        ])->validate();
        $contact=ContactInfo::first();
        if (isset($contact->id)){
            $contact->update($request->all());
        }
        ContactInfo::create($request->all());
        return response()->json('Successfully Updated',200);
    }
}
