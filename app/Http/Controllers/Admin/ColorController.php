<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ColorController extends Controller
{
    public function index(){
        $colors = Color::all();
        $bd=[];
        foreach ($colors as $brand){
            $brand->products;
            $bd[]= $brand;
        }
        return response()->json($bd,200);
    }
    public function add(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|unique:brands,name|max:191',
        ]);
        if ($val->fails()){
            return response()->json([0,'Invalid Request'],200);
        }
        $color = new Color();
        $color->name=$request->name;
        $color->save();
        return response()->json([1,'Successfully Added'],201);
    }
    public function update(Request $request,$id){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ]);
        //return response()->json($request->name,200);
        if ($val->fails()){
            return response()->json([0,'Invalid Request'],200);
        }
        $color = Color::findOrFail($id);
        if (isset($color->id)){
            $color->name=$request->name;
            $color->save();
            return response()->json([1,'Successfully updated'],200);
        }
        return response()->json([0,'Invalid Request'],200);
    }
    public function remove(Color $color){
        //return response()->json([0,$brand]);
        if ($color->products->count()>0)
            return response()->json([0,"This Color has already product you can't remove this."]);
        $color->delete();
        return response()->json([1,"Successfully Delete"],200);
    }
}
