<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        $cats =[];
        foreach ($categories as $cat){
            $cat->products->count();
            $cats[]=$cat;
        }
        return response()->json($cats,200);
    }
    public function add(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|unique:categories,name|max:191',
        ]);
        if ($val->fails()){
            return response()->json([0,'Invalid Request'],200);
        }
        $category = new Category();
        $category->name=$request->name;
        $category->save();
        return response()->json([1,'Successfully Added the category'],201);
    }
    public function update(Request $request,$id){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ]);
        //return response()->json($request->name,200);
        if ($val->fails()){
            return response()->json([0,'Invalid Request'],200);
        }
        $category = Category::findOrFail($id);
        if (isset($category->id)){
            $category->name=$request->name;
            $category->save();
            return response()->json([1,'Successfully updated'],200);
        }
        return response()->json([0,'Invalid Request'],200);
    }
    public function remove(Category $category){
        if ($category->products->count()>0)
            return response()->json([0,"This Category has already product yo can't remove this."]);
        $category->delete();
        return response()->json([1,"Successfully Delete"],200);
    }

}
