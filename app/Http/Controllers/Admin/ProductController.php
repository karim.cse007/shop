<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Color;
use App\SubCategory;
use App\Http\Controllers\Controller;
use App\Product;
use App\Size;
use App\SubSubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /*
     * method for get categories, brands, sizes, colors info get
     * */
    public function allInfo(){
        $categories=Category::all();
        $brands=Brand::all();
        $sizes=Size::all();
        $colors =Color::all();
        $info=([
           'categories'=>$categories,
           'brands'=>$brands,
           'sizes'=>$sizes,
           'colors'=>$colors,
        ]);
        return response()->json($info,200);
    }
    /*
     * method for get all products
     * */
    public function index(){
        $products = Product::with('category','brand','sizes','colors','subCategory','subSubCategory')->get();
        return response()->json($products,200);
    }

    /*
     * method for store product information
     * */
    public function add(Request $request){

        /*request data validate*/
        $this->valid($request->all())->validate();

        /*Validator::make($request->all(),[
            'image'=>'required|mimes:jpg,png,jpeg',
        ])->validate();*/

        $image = $request->file('image');

        $imgName = Str::slug($request->name).uniqid().'.'.$image->getClientOriginalExtension();
        if (!Storage::disk('public')->exists('product')){
            Storage::disk('public')->makeDirectory('product');
        }
        $img = Image::make($image)->resize(736,1000)->save();
        Storage::disk('public')->put('product/'.$imgName,$img);
        $code = 'P'.str_pad(mt_rand(1000,9999),4,4,STR_PAD_LEFT);
        $data = $request->all();
        unset($data['image']);
        $data +=[
            'image'=>$imgName,
            'code'=>$code,
        ];
        $ss = $product = Product::create($data);
        $product->sizes()->attach($request->sizes);
        $product->colors()->attach($request->colors);
        if (isset($ss)) return response()->json('Successfully add',201);
        return response()->json('invalid request',404);
    }
    /*
     * method for product update
     * */
    public function update(Request $request,Product $product){
        $this->valid($request->all())->validate();

        $image = $request->file('image');
        if (isset($image)){
            $imgName = Str::slug($request->name).uniqid().'.'.$image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('product')){
                Storage::disk('public')->makeDirectory('product');
            }
            if (Storage::disk('public')->exists('product/'.$product->image)){
                Storage::disk('public')->delete('product/'.$product->image);
            }
            $img = Image::make($image)->resize(736,1000)->save();
            Storage::disk('public')->put('product/'.$imgName,$img);
        }else{
            $imgName=$product->image;
        }
        $data = $request->all();
        unset($data['image']);
        $data +=[
            'image'=>$imgName,
        ];
        //return response()->json([0,'Invalid Request'],200);
        $ss = $product->update($data);
        $product->sizes()->sync($request->sizes);
        $product->colors()->sync($request->colors);

        if (isset($ss)) return response()->json('Successfully update',201);
        return response()->json('invalid request',404);
    }
    public function valid($data){
      return Validator::make($data,[
            'name'=>'required|string|max:191',
            'category_id'=>'required|exists:categories,id',
            'sub_category_id'=>'required|exists:sub_categories,id',
            'sub_sub_category_id'=>'required|exists:sub_sub_categories,id',
            'brand_id'=>'required|exists:brands,id',
            'sizes'=>'required|exists:sizes,id',
            'colors'=>'required|exists:colors,id',
            'price'=>'required|integer|not_in:0',
            'description'=>'required|string',
            'condition'=>'required|string',
        ]);
    }
    /*
     * method for remove product
     * */
    public function remove(Product $product){
        $product->sizes()->detach();
        $product->colors()->detach();
        if (Storage::disk('public')->exists('product/'.$product->image))
            Storage::disk('public')->delete('product/'.$product->image);
        $product->delete();
        return response()->json([1,"Successfully Delete"],200);
    }

    /*
     * method for get sub category by category id
     * */
    public function subCategory(Category $category){
        //return $category;
        $subCategory = $category->sub_categories;
        return response()->json($subCategory,200);
    }
    /*
     * method for get sub sub category by sub category id
     * */
    public function subSubCategory(SubCategory $subCategory){
        $subSubCategory = $subCategory->sub_sub_categories;
        return response()->json($subSubCategory);
    }

    /*
     * method for product stock status change
     * */
    public function stockStatusChange(Product $product)
    {
        if ($product->is_available ==1) $product->update(['is_available'=>0]);
        else $product->update(['is_available'=>1]);
        return response()->json('Successfully update status',200);
    }

    /*
     * method for product enable or disable status change
     * */
    public function enableDisableStatusChange(Product $product)
    {
        if ($product->is_disable ==1) $product->update(['is_disable'=>0]);
        else $product->update(['is_disable'=>1]);
        return response()->json('Successfully update status',200);
    }

}
