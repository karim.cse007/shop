<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SubCategory;
use App\SubSubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubSubCategoryController extends Controller
{
    public function index(){
        $sub_categories = SubSubCategory::all();
        $cats =[];
        foreach ($sub_categories as $cat){
            $cat->products->count();
            $cat->sub_category;
            $cat->category;
            $cats[]=$cat;
        }
        return response()->json($cats,200);
    }

    /*
     * method for sub sub category store
     * */
    public function add(Request $request){
        $this->val($request->all())->validate();
        $ss = SubSubCategory::create($request->all());
        if (isset($ss)) return response()->json('Successfully added',201);
        return response()->json('invalid request',404);
    }
    /*
     * method for sub sub category update
     * */
    public function update(Request $request,SubSubCategory $subSubCategory){
        $this->val($request->all())->validate();

        $ss= $subSubCategory->update($request->all());
        if (isset($ss)) return response()->json('Successfully updated',201);
        return response()->json('invalid request',404);
    }

    /*
     * method for sub sub category remove
     * */
    public function remove(SubSubCategory $sub_sub_category){
        //return response()->json([1,$sub_category],200);
        if ($sub_sub_category->products->count()>0)
            return response()->json([0,"This Sub-Category has already product you can't remove this."]);
        $sub_sub_category->delete();
        return response()->json([1,"Successfully Delete"],200);
    }

    public function val($data){
        return Validator::make($data,[
            'name'=>'required|string|unique:sub_categories,name|max:191',
            'category_id'=>'required|not_in:0|exists:categories,id',
            'sub_category_id'=>'required|not_in:0|exists:sub_categories,id',
        ]);
    }
}
