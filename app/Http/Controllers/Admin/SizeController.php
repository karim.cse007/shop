<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::all();
        $szs =[];
        foreach ($sizes as $sz){
            $sz->products->count();
            $szs[]=$sz;
        }
        return response()->json($szs,200);
    }
    public function add(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|unique:sizes,name|max:191',
        ]);
        if ($val->fails()){
            return response()->json([0,'Invalid Request'],200);
        }
        $size = new Size();
        $size->name=$request->name;
        $size->save();
        return response()->json([1,'Successfully Added'],201);
    }
    public function update(Request $request,$id){
        Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ])->validate();
        $size = Size::findOrFail($id);
        if (isset($size->id)){
            $size->name=$request->name;
            $size->save();
            return response()->json('Successfully updated',200);
        }
        return response()->json('Invalid Request',404);
    }
    public function remove(Size $size){
        //return response()->json([0,$brand]);
        if ($size->products->count()>0)
            return response()->json([0,"This Size has already product you can't remove this."]);
        $size->delete();
        return response()->json([1,"Successfully Delete"],200);
    }
}
