<?php

namespace App\Http\Controllers\Admin;
use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        $bd=[];
        foreach ($brands as $brand){
            $brand->products;
            $bd[]= $brand;
        }
        return response()->json($bd,200);
    }
    public function add(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|unique:brands,name|max:191',
        ]);
        if ($val->fails()){
            return response()->json('Invalid Request',200);
        }
        $brand = new Brand();
        $brand->name=$request->name;
        $brand->save();
        return response()->json('Successfully Added',201);
    }
    public function update(Request $request,$id){
        Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ])->validate();
        $brand = Brand::findOrFail($id);
        if (isset($brand->id)){
            $brand->name=$request->name;
            $brand->save();
            return response()->json('Successfully updated',200);
        }
        return response()->json('Invalid Request',404);
    }
    public function remove(Brand $brand){
        //return response()->json([0,$brand]);
        if ($brand->products->count()>0)
            return response()->json([0,"This Brand has already product you can't remove this."]);
        $brand->delete();
        return response()->json([1,"Successfully Delete"],200);
    }
}
