<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function user()
    {
        $user = Auth::user();
        return response()->json($user,200);
    }
    public function passwordUpdate( Request $request){

        $val = Validator::make($request->all(),[
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($val->fails()){
            return response()->json($val->getMessageBag()->all(),200);
        }
        $hashpassword = Auth::user()->password;

        if(Hash::check($request->old_password, $hashpassword)){
            if (!Hash::check($request->password,$hashpassword)){
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                return response()->json(['Success ! Successfully change the password'],200);
            }else{
                return response()->json(['New password cannot be the same as old password']);
            }
        }else{
            return response()->json(['Old password not match!!'],200);
        }
    }
}
