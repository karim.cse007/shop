<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function getReviews($id){
        $product =Product::where('id',$id)->first();
        if (isset($product)){
            $reviews= $this->getReviewUser($product->reviews);
            //return $reviews;
            return response()->json($reviews,200);
        }
        $reviews = [];
        return response()->json($reviews,200);
    }
    public function getReviewUser($reviews){
        $rv=[];
        foreach ($reviews  as $review){
            $review->user;
            $rv[]=$review;
        }
        return $rv;
    }
    public function setReview(Request $review){
        $result = Review::create($review->all());
        if (isset($result))
            return response()->json(true,200);
        else
            return response()->json(false,200);
    }
}
