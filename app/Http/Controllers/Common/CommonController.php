<?php

namespace App\Http\Controllers\Common;

use App\AboutUs;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function categorySearch(){
        return view('category');
    }
    /*
     * method for get about us information
     * */
    public function aboutUs()
    {
        return response()->json(AboutUs::first(),200);
    }
}
