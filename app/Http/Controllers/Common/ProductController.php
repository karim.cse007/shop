<?php

namespace App\Http\Controllers\Common;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /*
     * method for get all products with out disabled products
     * */
    public function index(){
        $products = Product::with([
            'category'=>function($q){$q->select('id','name');},
            'brand'=>function($q){$q->select('id','name');},
            'sizes',
            'subCategory'=>function($q){$q->select('id','name');},
            'colors',
            'subSubCategory'=>function($q){$q->select('id','name');},
        ])->where(['is_disable'=>0])->inRandomOrder()->get();
        return response()->json($products,200);
    }

    /*
     * method for take random products
     * */
    public function randProduct(){
        $products = Product::with([
            'category'=>function($q){$q->select('id','name');},
            'brand'=>function($q){$q->select('id','name');},
            'sizes',
            'subCategory'=>function($q){$q->select('id','name');},
            'colors',
            'subSubCategory'=>function($q){$q->select('id','name');},
        ])->where(['is_disable'=>0])->take(5);
        return response()->json($products,200);
    }

    /*
     * method for products get with category brand sizes, colors sub category and sub sub category
     * */
    public function product($products){
        $pd=[];
        foreach ($products as $product){
            $product->category;
            $product->brand;
            $product->sizes;
            $product->colors;
            $product->subCategory;
            $product->subSubCategory;
            $pd[] =$product;
        }
        return $pd;
    }
    public function relatedProducts(Category $category){
        return response()->json($this->product($category->products),200);
    }

    /*
     * product find by header selection
     * */
    public function findByHeader(Request $request){
        //return response()->json($request[0],200);
        if ($request[0]==0){
            $products = Category::findOrFail($request[1])->products
                ->where('sub_category_id',$request[2]);
            if (isset($products))
                return response()->json($this->product($products),200);
            else
                return response()->json([],200);
        }elseif ($request[0]==1){
            $products = SubCategory::findOrFail($request[1])->products
                ->where('sub_sub_category_id',$request[2]);
            if (isset($products))
                return response()->json($this->product($products),200);
            else
                return response()->json([],200);
        }
        return response()->json([],200);
    }

    /*
     * method for product search  by product name
     * */
    public function productSearch($name){
        //return response()->json([],200);
        $products=Product::where('name','LIKE','%'.$name.'%')->get();
        if (isset($products)){
            return response()->json($this->product($products),200);
        }
        return response()->json([],200);
    }
}
