<?php

namespace App\Http\Controllers\Common;

use App\ContactInfo;
use App\Http\Controllers\Controller;
use App\LogoName;
use App\Social;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FooterController extends Controller
{
    public function allInfo(){
        $social=Social::first();
        $contact=ContactInfo::first();
        $logoName=LogoName::first();
        $infos=[
            'social'=>$social,
            'contact'=>$contact,
            'logoName'=>$logoName,
        ];
        return response()->json($infos,200);
    }
    public function subscribe(Request $request){

        $val= Validator::make($request->all(),[
            'email'=>'required|email|max:191|unique:subscribers,email',
        ]);
        if ($val->fails()){
            return response()->json([0,$val->getMessageBag()->first()],200);
        }
        Subscriber::create($request->all());
        return response()->json([1,'You are now subscriber'],200);
    }
}
