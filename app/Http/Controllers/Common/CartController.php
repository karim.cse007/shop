<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function addProduct(Request $product){

        \Cart::add(array(
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => 1,
            'attributes' => array(
                'image' => $product->image,
                'color'=>'',
                'size'=>'',
            ),
        ));
        return response()->json([1,'product is added to your cart'],200);
    }
    public function getCart(){
        $content = \Cart::getContent();
        $total = \Cart::getTotal();
        $info=[
            'content'=>$content,
            'total'=>$total,
        ];
        return response()->json($info,200);
    }
    public function cartUpdate(Product $product, int $quantity){
        $cartCollections = \Cart::get($product->id);
        //return $quantity;
        if (isset($cartCollections)){
            if($quantity >0){
                \Cart::update($product->id, array(
                    'quantity' => array(
                        'relative' => false,
                        'value' => $quantity,
                    ),
                ));
            }elseif ($quantity.integerValue()<1)
                $this->cartRemove($product->id);
        }
        return $this->getCart();
    }
    public function cartRemove($id){
        \Cart::remove($id);
        return $this->getCart();
    }
    public function valid($data)
    {
        return Validator::make($data,[
            'id'=>'required|not_in:0|exists:products,id',
            'name'=>'required|string|max:191',
            'image'=>'required|string|max:191',
        ]);
    }
}
