<?php

namespace App\Http\Controllers\Common;

use App\Category;
use App\ContactInfo;
use App\Http\Controllers\Controller;
use App\LogoName;
use App\Social;
use Illuminate\Http\Request;

class HeaderController extends Controller
{
    /*
     * method for get all header and footer information
     * */
    public function allInfo(){
        $logoName = LogoName::first(['logo','name']);
        $social=Social::first();
        $contact=ContactInfo::first();
        $categories = Category::with([
            'sub_categories'=>function($q){$q->with('sub_sub_categories');}
        ])->get(['id','name']);
        $allInfo =[
            'logoName'=>$logoName,
            'categories'=>$categories,
            'social'=>$social,
            'contact'=>$contact,
        ];
        return response()->json($allInfo,200);
    }
}
