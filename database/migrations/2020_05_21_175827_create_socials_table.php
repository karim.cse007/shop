<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->id();
            $table->string('facebook')->default('javascript:void(0)');
            $table->string('tweeter')->default('javascript:void(0)');
            $table->string('linkedin')->default('javascript:void(0)');
            $table->string('skype')->default('javascript:void(0)');
            $table->string('youtube')->default('javascript:void(0)');
            $table->string('instagram')->default('javascript:void(0)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
