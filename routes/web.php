<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return "Cleared!";
});
Auth::routes();
Route::get('/', function () {
    return view('welcome');
});
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/header/all/info','Common\HeaderController@allInfo');
Route::get('/footer/all/info','Common\FooterController@allInfo');
Route::post('/subscriber/subscribe','Common\FooterController@subscribe');

Route::get('/products/all/info','Common\ProductController@index');
Route::get('/products/random','Common\ProductController@randProduct');
Route::get('/product/related/products{category}','Common\ProductController@relatedProducts');
Route::post('/product/find/by/header','Common\ProductController@findByHeader');
Route::get('/product/searsch/{name}','Common\ProductController@productSearch');

Route::get('/category/search','Common\CommonController@categorySearch');
Route::get('/about/us/info/get','Common\CommonController@aboutUs');

Route::post('/product/add/cart','Common\CartController@addProduct');
Route::get('/product/update/{product}/cart{quantity}','Common\CartController@cartUpdate');
Route::get('/product/get/cart/info','Common\CartController@getCart');
Route::get('/product/cart/remove{id}','Common\CartController@cartRemove');
Route::get('/product/reviews{id}','User\ReviewController@getReviews');

Route::group(['middleware'=>['auth','history']],function (){
    Route::post('/product/review/set','User\ReviewController@setReview');
    Route::post('/user/password/change','Auth\PasswordChangeController@changePassword');
});

Route::group(['as'=>'user.','prefix'=>'user','namespace'=>'User','middleware'=>['auth','user','history']],function (){
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/info', 'ProfileController@user')->name('info');
    Route::post('/change/password', 'ProfileController@passwordUpdate')->name('info');
});

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin','history']],function (){
    Route::get('/dashboard','ComonController@index')->name('dashboard');

    Route::get('/logo/name','ComonController@logoName')->name('logo.name');
    Route::post('/logo/name/update','ComonController@logoNameUpdate')->name('logo.name.update');

    Route::get('/social','ComonController@social')->name('social');
    Route::post('/social/update','ComonController@socialUpdate')->name('social.update');

    Route::get('/category','CategoryController@index')->name('category');
    Route::post('/category/add','CategoryController@add')->name('category.add');
    Route::post('/category/update{id}','CategoryController@update')->name('category.update');
    Route::delete('/category/delete/{category}','CategoryController@remove')->name('category.remove');

    Route::get('/sub/category','SubCategoryController@index')->name('sub.category');
    Route::post('/sub/category/add','SubCategoryController@add')->name('sub.category.add');
    Route::post('/sub/category/update{sub_category}','SubCategoryController@update')->name('sub.category.update');
    Route::delete('/sub/category/delete/{sub_category}','SubCategoryController@remove')->name('sub.category.remove');

    Route::get('/sub/sub/category','SubSubCategoryController@index')->name('sub.sub.category');
    Route::post('/sub/sub/category/add','SubSubCategoryController@add')->name('sub.sub.category.add');
    Route::post('/sub/sub/category/update{sub_sub_category}','SubSubCategoryController@update')->name('sub.sub.category.update');
    Route::delete('/sub/sub/category/delete/{sub_sub_category}','SubSubCategoryController@remove')->name('sub.sub.category.remove');

    Route::get('/brand','BrandController@index')->name('brand');
    Route::post('/brand/add','BrandController@add')->name('brand.add');
    Route::post('/brand/update{id}','BrandController@update')->name('brand.update');
    Route::delete('/brand/delete/{brand}','BrandController@remove')->name('brand.remove');

    Route::get('/size','SizeController@index')->name('size');
    Route::post('/size/add','SizeController@add')->name('size.add');
    Route::post('/size/update{id}','SizeController@update')->name('size.update');
    Route::delete('/size/delete/{size}','SizeController@remove')->name('size.remove');

    Route::get('/color','ColorController@index')->name('color');
    Route::post('/color/add','ColorController@add')->name('color.add');
    Route::post('/color/update{id}','ColorController@update')->name('color.update');
    Route::delete('/color/delete/{color}','ColorController@remove')->name('color.remove');

    Route::get('/product/all/info','ProductController@allInfo')->name('product.all.info');
    Route::get('/product','ProductController@index')->name('product');
    Route::post('/product/add','ProductController@add')->name('product.add');
    Route::post('/product/update{product}','ProductController@update')->name('product.update');
    Route::delete('/product/delete/{product}','ProductController@remove')->name('product.remove');
    Route::get('/product/sub/category/{category}','ProductController@subCategory');
    Route::get('/product/sub/sub/category/{sub_category}','ProductController@subSubCategory');
    Route::put('/product/available/status/change{product}','ProductController@stockStatusChange');
    Route::put('/product/disable/status/change{product}','ProductController@enableDisableStatusChange');

    Route::get('/about/us','ComonController@aboutUs')->name('about.us');
    Route::post('/about/us/update','ComonController@aboutUsUpdate')->name('about.us.update');

    Route::get('/contact/info','ComonController@contactInfo')->name('contact.info');
    Route::post('/contact/info/update','ComonController@contactUpdate')->name('contact.info.update');

});
/*Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');*/
